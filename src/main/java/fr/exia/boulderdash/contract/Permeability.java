package fr.exia.boulderdash.contract;

/**
 * <h1>The Permeability Enum.</h1>
 *
 * @author Groupe 6
 * @version 0.1
 */

public enum Permeability {

    /**
     * The blocking.
     */
    BLOCKING,
    /**
     * The penetrable.
     */
    PENETRABLE,
    /**
     * The destructible.
     */
    DESTRUCTING,
    /**
     * The collectible.
     */
    COLLECTIBLE,
    /**
     * The killing.
     */
    KILLING;
}

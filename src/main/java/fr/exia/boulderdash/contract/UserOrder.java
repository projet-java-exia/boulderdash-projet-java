package fr.exia.boulderdash.contract;

/**
 * <h1>The Enum UserOrder.</h1>
 *
 * @author Groupe 6
 * @version 0.1
 */

public enum UserOrder {

    /**
     * The right.
     */
    RIGHT,

    /**
     * The left.
     */
    LEFT,

    /**
     * The down.
     */
    DOWN,

    /**
     * The up.
     */
    UP,

    /**
     * The nop.
     */
    NOP
}

/**
 * This package contains classes to start the game.
 * The main() function is in the BoulderDash class.
 *
 * @author Groupe 6
 * @version 0.1
 */

package fr.exia.boulderdash.main;

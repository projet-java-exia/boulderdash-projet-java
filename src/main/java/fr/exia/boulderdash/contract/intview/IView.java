package fr.exia.boulderdash.contract.intview;

/**
 * <h1>The Interface IView.</h1>
 *
 * @author Groupe 6
 * @version 0.1
 */

public interface IView {

    /**
     * Display message.
     *
     * @param message the message
     */
    void displayMessage(String message);

    /**
     * Follow MyPlayer.
     */
    void followMyPlayer();
}

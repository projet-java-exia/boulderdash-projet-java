/**
 * This package contains classes to produce MotionLessElement.
 * There is a factory : MotionlessElementsFactory.
 *
 * @author Groupe 6
 * @version 0.1
 */

package fr.exia.boulderdash.entity.motionless;
